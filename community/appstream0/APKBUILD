# Contributor: Leo <thinkabit.ukim@gmail.com>
# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Pablo Correa Gomez <ablocorrea@hotmail.com>
pkgname=appstream0
pkgver=0.16.3
pkgrel=1
pkgdesc="Provides a standard for creating app stores across distributions"
url="https://distributions.freedesktop.org/wiki/AppStream"
arch="all"
license="LGPL-2.1-or-later"
makedepends="
	appstream-cli
	cairo-dev
	curl-dev
	fontconfig-dev
	gdk-pixbuf-dev
	glib-dev
	gobject-introspection-dev
	gperf
	itstool
	librsvg-dev
	libxml2-dev
	libxmlb-dev
	meson
	pango-dev
	qt5-qtbase-dev
	yaml-dev
	"
triggers="$pkgname.trigger=/usr/share/app-info/*:/var/cache/swcatalog/xml"
install="$pkgname.post-install $pkgname.post-upgrade"
subpackages="
	$pkgname-dbg
	$pkgname-dev
	$pkgname-doc
	$pkgname-qt
	$pkgname-compose
	$pkgname-lang
	"
source="
	https://www.freedesktop.org/software/appstream/releases/AppStream-$pkgver.tar.xz
	disable_test_read_fontinfo.patch
	"
builddir="$srcdir/AppStream-$pkgver"

build() {
	abuild-meson \
		-Db_lto=true \
		-Dqt=true \
		-Dsystemd=false \
		-Ddocs=false \
		-Dapidocs=false \
		-Dinstall-docs=false \
		-Dstemming=false \
		-Dvapi=false \
		-Dapt-support=false \
		-Dgir=true \
		-Dcompose=true \
		. output
	meson compile -C output
}

check() {
	XDG_RUNTIME_DIR="$builddir" \
	meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output

	# Remove installed-tests, there is no switch to disable installing them
	rm -rf "$pkgdir"/usr/share/installed-tests

	# Remove cli application, so this doesn't conflict with appstream-cli
	rm -rf "$pkgdir"/usr/bin/appstreamcli
}

compose() {
	pkgdesc="Building blocks to compose AppStream metadata"

	amove usr/lib/libappstream-compose*
	amove usr/lib/girepository-*/AppStreamCompose*

	# Needs so:libappstream-compose.so and thus needs
	# to be moved to prevent a recursive dependency.
	amove usr/libexec/appstreamcli-compose
}

qt() {
	pkgdesc="Qt5 interface for AppStream"

	amove usr/lib/libAppStreamQt.so.*
}

sha512sums="
bb356d69438674a65ffb2ece4692383066703a9f3b1206d2408155cc15f0b6002e312071fef0669516ea9642752567ceb3fc7030ce8c9a3d888f8df0ad7116ef  AppStream-0.16.3.tar.xz
1caa6ed836316767a69db5c0465681ded1cac261ef51ab195c8ed1f1648fc4f5bce29e158fecd10294eb81b04fef29c7ddf86cd7c6372d94b08c7bf8da682bcf  disable_test_read_fontinfo.patch
"
